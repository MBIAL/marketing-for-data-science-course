import numpy as np
import pandas as pd
from sklearn.metrics import euclidean_distances
from sklearn.model_selection import StratifiedKFold


def mia_index(k, inertie):
    """mean index adequacy: mesure la compacité au sein des clusters, plus il est petit mieux c'est.
    k: nombre de clusters
    inertie: l'inertie totale du clustering
    """
    mia=np.sqrt(inertie/k)
    return(mia)

def cdi_index(k, inertie, centers):
    """cluster dispersion indicator: permet de mesurer à la fois la compacité des clusters et la mesure dans laquelle chaque cluster diffère des autres.
     Une valeur plus faible pour CDI suggère une meilleure solution de clustering.
    k: nombre de clusters
    inertie: l'inertie totale du clustering
    centers: les centres de chaque cluster
    """
    dists = euclidean_distances(centers)
    tri_dists = dists[np.triu_indices(k, 1)]
    cdi=np.sqrt(inertie/k)/(tri_dists.mean())   
    return(cdi)

def min_max(X):
    X=(X-X.min())/(X.max()-X.min())
    return (X)
