from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score, davies_bouldin_score, calinski_harabasz_score
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from utils import mia_index, cdi_index
import numpy as np
import pandas as pd
import plotly.express as px 

class Kmeans:
    def __init__(self,data):
        self.data = data

    def elbow_inertia(self, k):
        """Permet de choisir le nombre de clusters maximal avec la méthode du coude
        k: valeur à ittérer pour le nombre de clusters
        """
        wcss = []
        for i in range(1, k):
            if i%10 ==0:
                print(i)
            kmeans = KMeans(n_clusters=i, init='k-means++', max_iter=200, n_init=10, random_state=0)
            kmeans.fit(self.data)
            wcss.append(kmeans.inertia_)
        
        fig = px.scatter(x=range(1,k),y=wcss)
        fig.show()
    
    def cross_validation(self,k):
        """Sémi-validation croisée entre plusieurs indices afin de choisir le nombre final de clusters
        k: valeur à ittérer pour le nombre de clusters
        """
        Imiadf=[]
        Icdidf=[]
        Idbidf=[]
        Isildf=[]
        Ichsdf=[]

        for i in range(2,k):
            if i%2 ==0:
                print(i)
            kmeans = KMeans(n_clusters=i, init='k-means++', max_iter=200, n_init=10, random_state=0)
            kmeans.fit(self.data)
            Imiadf.append(mia_index(i, kmeans.inertia_))
            Icdidf.append(cdi_index(i, kmeans.inertia_,kmeans.cluster_centers_))
            Idbidf.append(davies_bouldin_score(self.data,kmeans.labels_))
            Isildf.append(silhouette_score(self.data, kmeans.labels_))
            Ichsdf.append(calinski_harabasz_score(self.data, kmeans.labels_))

            #Graphe1
        fig,ax = plt.subplots()
        ax.plot(range(2, k),Imiadf, label='MIA')
        ax.plot(range(2, k),Icdidf, label='CDI')
        plt.title('Validation MIA, CDI, Calinski-Harabasz')
        plt.xlabel('Nombre de clusters')
        plt.ylabel('Indices')
        plt.legend(loc='lower left')
        ax2=ax.twinx()
        ax2.plot(range(2, k),Ichsdf,color="red",linewidth=3, label='Calinski-Harabasz')
        plt.legend()
        graphe1=plt.show()
        graphe1

            #Graphe2
        fig,ax = plt.subplots()
        ax.plot(range(2, k),Idbidf, label='DBI')
        plt.title('Validation DBI, Silhouette')
        plt.xlabel('Nombre de clusters')
        plt.ylabel('Indices')
        plt.legend()
        ax2=ax.twinx()
        ax2.plot(range(2, k),Isildf, color='brown',label='Silhouette')
        plt.legend()
        graphe2=plt.show()
        graphe2
    
    def kmeans_fit(self,k):
        """clustering final
        k: nombre de clusters
        """
        kmeans = KMeans(n_clusters=k, init='k-means++', max_iter=200, n_init=10, random_state=0)
        kmeans.fit(self.data)
        cluster=pd.DataFrame()
        cluster['code_article']=self.data.index
        cluster['Clusters']=kmeans.labels_+1
        cluster.index = cluster.code_article
        cluster_dict = cluster.to_dict()
        cluster_dict['code_article'] = cluster_dict.pop('Clusters')
        return kmeans, cluster,cluster_dict
    
    def clusters_projection(self,kmeans):
        """projection des clusters avec l'ACP
        """
        pca =  PCA(n_components=2)
        pca_df = pca.fit_transform(self.data)
        ACP_proj= pd.DataFrame(data = pca_df 
                    , columns = ['composante principale 1', 'composante principale 2'])
        ACP_proj['cluster']=kmeans.labels_

        ### Représentation
        plt.figure()
        plt.figure(figsize=(10,10))
        plt.xticks(fontsize=12)
        plt.yticks(fontsize=14)
        plt.xlabel('Projection axe 1',fontsize=20)
        plt.ylabel('Projection axe 2',fontsize=20)
        l=['0','1','2']
        plt.title("Analyse en composantes principales",fontsize=20)
        plt.scatter(ACP_proj['composante principale 1'], ACP_proj['composante principale 2'], c=ACP_proj['cluster'])
        plt.legend();
        return pca

    def fit(self,k):
        kmeans, cluster,cluster_dict = self.kmeans_fit(k)
        pca = self.clusters_projection(kmeans)
        return kmeans, cluster,cluster_dict, pca